/// <reference types = "Cypress"/>
class LoginCredentials
{
    username(){
        return cy.get('#UserName')
    }
    password(){
        return cy.get('#Password')
    }
    loginbtn(){
        return cy.get('.btn').click()
    }
}

export default LoginCredentials
