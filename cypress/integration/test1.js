/// <reference types="Cypress" />

/*describe('Login and create branch', () =>
{

it('Login',() => {

//Visit  url
cy.visit("http://wallbee.codebeestaging.com")
//Login
cy.get('#UserName').type('wallbeeadmin')
cy.get('#Password').type('Wallbee123!')
cy.get('.btn').click()
cy.wait(2000)
})


it('verify page navigation',() => {

    cy.url().should('include', 'http://wallbee.codebeestaging.com/Shipment/Create')
    
    })

it('Create branch',() => {


//Click on Branch from the menu bar
cy.get('.Branch').click()
cy.wait(2000)
//Click on the Create button
cy.get('.btn').click()
//Enter branch Name
cy.get('#Name').type ('Branch Name new1')
// Enter branch value
cy.get('#BranchValue').type('1')
//Enter route
cy.get('#Route').type('11')
//Enter serial
cy.get('#Serial').type('111')
//Enter Address1
cy.get('#Address_Addressline1').type('Kastrup')
//Select Country
cy.get('#Address_CountryCode').select('US')
//Select state
cy.get('#Address_StateCode').select('CA')
//Enter city
cy.get('#Address_City').type('Kastrup')
//Enetr email
cy.get('#Address_Email1').type('beenu@codebee.dk')
//Click on create button
cy.get('.btn').click()
})
it('Verify branch creation',() => {
   // cy.title.should('eq', 'BRANCHES')
   cy.title().should('eq', 'My Awesome Application').contains('Branch Name new1')

})
})*/
describe('Login and create branch', () =>
{

before('Login',() => {

//Visit  url
cy.visit("https://react-redux.realworld.io/#/login")
//Login
cy.get('input[type="email"]').type('shresthabeenu@gmail.com')
cy.get('input[type="password"]').type('123456789')
cy.get('.btn').contains('Sign in').should('be.visible').click()
cy.wait(2000)
})

    it('Create a post', function () {
        cy.get('ul.navbar-nav').children().contains('New Post').click()
        cy.hash().should('include', '#/editor')
        cy.get('form').within(($form) => {
            cy.get('input').first().type('Test')
            cy.get('input').eq(1).type('Test 1')
            cy.get('textarea').last().type('Test 2')
            cy.contains('Publish Article').click()
        })
        cy.url().should('include', 'article')
    })

    it('Mark-unmark as favorite', function () {
        cy.get('ul.navbar-nav').children().contains('QAMs').click()
        cy.contains('My Articles').should('be.visible')
        cy.get('.ion-heart').first().click()
        cy.contains('Favorited Articles').click()
        cy.url().should('include', 'favorites')
        cy.get('.btn-primary').first().then(($fav) => {
            const favCount = $fav.text()
            expect(parseInt(favCount)).to.eq(1)
        }).click()
        cy.reload()
        cy.contains('No articles are here... yet.').should('be.visible')
        cy.go('back')
    })
})




