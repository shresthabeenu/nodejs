describe('API Testing', () => {
    
    Cypress.config('baseUrl', 'http://dummy.restapiexample.com/api/v1')
    it('PUT - update', () => {
        const item = {"name":"test1"}
        cy.request({method:'PUT', url:'/update/1', body:item, failOnStatusCode: false}).its('status').should('eq', 401)
    })
})