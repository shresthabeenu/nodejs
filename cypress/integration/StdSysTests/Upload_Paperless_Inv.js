describe('Create paperless shipment from clone shipment', () =>{

    before(function() {
        cy.Login()
        cy.viewport(1500,1000)
    })

    beforeEach('Navigate to clone shipment page',() => {

        cy.visit(Cypress.env('clone_ups'))

    })

    beforeEach('Verify clone page navigation',() => {

        cy.url().should('include', 'CloneAndDelete=false')
        cy.log('The clone page is navigated')

    })

    it('Upload invoice file on paperless', () => {
        
        const filepath = 'air1.pdf'
        cy.get('#Shipment_PaperlessFile').attachFile(filepath).click()
        cy.log('test')
        cy.get('.form-fill-file').children().contains('air1.pdf')

    })



})