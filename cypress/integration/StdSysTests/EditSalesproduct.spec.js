/// <reference types = "Cypress"/>
import LoginPage from '../StdSysTests/LoginPage'


describe('select products from account page',()=>{
    before('Login to shipment page',()=>{

        const login = new LoginPage()
        login.visit()
        login.fillusername('wallbeeadmin')
        login.fillpassword('Wallbee123!')
        login.submit()
        cy.title().should('be.equal','WALLBEE - Create Shipment')


    })

    beforeEach('Redirect to the sale product page',()=>{

        cy.visit('http://wallbee.codebeestaging.com/SalesProduct/Edit/3255')
        // cy.get('.ui-dialog-buttons > .ui-dialog-titlebar > .ui-dialog-titlebar-close').click()
        // cy.get('.Products').trigger('mouseover')
        // cy.get('div.position-fix:nth-child(1) header.head nav.navi div.container-fluid ul.big-nav li.dropdown:nth-child(6) > ul.dropdown-menu').invoke('show')
        // cy.get('.SalesProduct').click()
        
    })

    it ('Create sale product',()=>{
        //cy.get('.page-title > .btn').click()
        cy.get('#TransportCompanyId').select('DHL Freight SE')
        cy.get('#btnSave').click()
        //cy.get('tbody > :nth-child(n)').should('eq', 'tets')
        cy.title().should('be.equal','WALLBEE - Sale Products')

})

})
