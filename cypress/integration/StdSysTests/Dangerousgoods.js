describe('Create shipment with DG', function() {
    

    before('Login',() => {
    
    //Visit  url
    cy.visit(Cypress.env('url'))
    cy.viewport(1600, 1000)
    //Login
    cy.get('#UserName').type(Cypress.env('username'))
    cy.get('#Password').type(Cypress.env('password'))
    cy.get('.btn').click()
    cy.wait(2000)
})

    before('Redirect to the clone shipment page',() => {
        
        cy.visit(Cypress.env('clone_asendia'))
        cy.wait(3000)

    })

        beforeEach('Verify clone page navigation',() => {

            cy.url().should('include', 'CloneAndDelete=false')
            cy.log('The clone page is navigated')

        })

        beforeEach('Select Dangerous Goods',() => 
        {
            cy.get('.DangerousGoodsContainer > .form-group > .switch-button-background > .switch-button-button').click()
            cy.get('#Shipment_Quantity').type('11')
            cy.get('#Shipment_UnNumber').type('12')
            cy.get('#Shipment_DangerousGoodsTypeID > option').eq(7).then(element => cy.get('#Shipment_DangerousGoodsTypeID').select(element.val()))
            //cy.get('#Shipment_DangerousGoodsTypeID').select('25')
            cy.get('#Shipment_ReceiversReference').type('7')
            cy.get('#Shipment_Quantity').type('5')
            cy.get('#Shipment_UnNumber').type('2')
        })

        it('create shipment', () => {

            //cy.get('.paperlesscontainer > :nth-child(2) > :nth-child(1) > .switch-button-background > .switch-button-button').click()
            cy.wait(5000)
            cy.get('#btnShowShippingOptions').click()
            cy.wait(20000)
            cy.scrollTo('bottom', {
                duration: 10000,
                easing: 'swing',
            })
           
            //cy.get('.table').should('be.visible').contains('tr.category-1 td','GLS_Euro_Business_Parcel multi').click()
            //cy.wait(5000)
            cy.get('#Shipment_ListOfShipmentBillInfo_3__isSelected').check()
            cy.get('#SubmitButtonContainer > .btn').click()
            cy.url().should('include','ShipmentAWB?ids=')
        })
        })

    

        
    
        


    