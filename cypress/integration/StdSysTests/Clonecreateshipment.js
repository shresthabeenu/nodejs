describe('Create shipment with DG', function() {
    

    before('Login',() => {
    
    //Visit  url
    cy.visit(Cypress.env('url'))
    cy.viewport(1600, 1000)
    //Login
    cy.get('#UserName').type(Cypress.env('username'))
    cy.get('#Password').type(Cypress.env('password'))
    cy.get('.btn').click()
    cy.wait(2000)
})

    before('Redirect to the clone shipment page',() => {
        
        cy.visit(Cypress.env('clone_dhl_frieght_se'))
        cy.wait(3000)

    })

        beforeEach('Verify clone page navigation',() => {

            cy.url().should('include', 'CloneAndDelete=false')
            cy.log('The clone page is navigated')
            //Enter	Unit Price on commodity paperless
            //cy.get('.editor-field').eq(4).click().type('2')
        })

        it('create shipment', () => {

            cy.get('#Shipment_SendersReference').type('')
            cy.get('#btnShowShippingOptions').click()
            cy.wait(20000)
            cy.scrollTo('bottom', {
                duration: 10000,
                easing: 'swing',
            })
            cy.get('#Shipment_ListOfShipmentBillInfo_21__isSelected').check()
            cy.get('#SubmitButtonContainer > .btn').click()
            cy.url().should('include','ShipmentAWB?ids=')

        })
        })

    

        
    
        


    