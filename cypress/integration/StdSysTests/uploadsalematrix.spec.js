/// <reference types = "Cypress"/>
import LoginPage from '../StdSysTests/LoginPage'
import 'cypress-file-upload'

describe('select products from account page',()=>{
    before('Login to shipment page',()=>{

        const login = new LoginPage()
        login.visit()
        login.fillusername('wallbeeadmin')
        login.fillpassword('Wallbee123!')
        login.submit()
        cy.title().should('be.equal','WALLBEE - Create Shipment')


    })

    beforeEach('Redirect to the sale matrix page',()=>{

        cy.visit(Cypress.env('salematrix'))
    })

    it ('Upload matrix sale',()=>{
      
        // cy.get('.page-title > .btn').click()
        // cy.get('#CompanyID').select('Leman')
        // cy.wait(5000)
        cy.get('#SaleProductID').select('DHL_Freight_SE_API_Stycke_G12')
        cy.get('#MatrixTypeID').select('PriceSale')
        // cy.fixture('SaleMatrix.csv').then(fileContent => {
        //     cy.get('#exampleInputFile').attachFile({
        //         fileContent: fileContent.toString(),
        //         fileName: 'SaleMatrix.csv',
        //         mimeType: 'text/csv'
        //     })
        // })

            
        cy.get('#exampleInputFile').attachFile('SaleMatrix.csv')
        cy.get('#MatrixUploadForm > .btn').click()
        cy.get('.col-md-4 > .btn').click()
        cy.title().should('be.equal','WALLBEE - Matrix Sale')
    })

})


