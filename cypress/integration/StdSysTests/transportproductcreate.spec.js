/// <reference types = "Cypress"/>
import LoginPage from '../StdSysTests/LoginPage'


describe('select products from account page',()=>{
    before('Login to shipment page',()=>{

        const login = new LoginPage()
        login.visit()
        login.fillusername('wallbeeadmin')
        login.fillpassword('Wallbee123!')
        login.submit()
        cy.title().should('be.equal','WALLBEE - Create Shipment')


    })

    beforeEach('Redirect to the transport product page',()=>{

        cy.get('.ui-dialog-buttons > .ui-dialog-titlebar > .ui-dialog-titlebar-close').click()
        cy.visit(Cypress.env('transport_product'))
        cy.wait(1000)
        //cy.get('div.position-fix:nth-child(1) header.head nav.navi div.container-fluid ul.big-nav li.dropdown:nth-child(6) > ul.dropdown-menu').invoke('show')
        //cy.get('.SalesProduct').click()
        
    })

    it ('Create sale product',()=>{
        //cy.get('.page-title > .btn').click()
        cy.get('#InternalName').type('DHL_Freight_SE_API_Stycke_G12')
        cy.get('#DisplayName').type('DHL_Freight_SE_API_Stycke_G12')
        cy.get('#CompanyTypeID').select('DHL Freight SE')
        //cy.get(':nth-child(3) > .switch-button-background > .switch-button-button').click()
        cy.get('#CurrencyCode').select('Danish Krone')
        cy.get('#TransportNumber').clear().type('1000')
        cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Stycke_G12')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Pall')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Home_Delivery')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Stycke')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Parti')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Intl_Paket_Export')
       // cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Intl_Parcel_Connect')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Intl_Parcel_Connect_Return')
        // cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Intl_Eurapid')
       // cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Intl_Euroconnect')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Intl_Euroconnect_Plus')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Paket_G7')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Paket_G10')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Paket_G12')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Pall_G7')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Pall_G10')
         //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Pall_G12')
        //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Stycke_G7')
         //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Stycke_G10')
          //cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Stycke_G12 ')
        cy.get('#WallServiceProductID').select('DHL_Freight_SE_API_Service_Point')
        cy.get('#MaxWeights').clear().type('1000')
        cy.get('#MaxVolWeights').clear().type('1000')
        cy.get('#MaxColliWeights').clear().type('1000')
        cy.get('#MaxColliVolWeights').clear().type('1000')
        cy.get('#MaxHeight').clear().type('1000')
        cy.get('#MaxWidth').clear().type('1000')
        cy.get('#MaxLength').clear().type('1000')
        cy.get('#MaxGrid').clear().type('1000')
        cy.get('#InvoiceMargin').clear().type('1000')
        cy.get('.btn').click()
        cy.title().should('be.equal','WALLBEE - Transport Products')

})

})
