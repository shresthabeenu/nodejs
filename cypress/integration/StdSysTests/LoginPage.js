//PageObjLogin.spec.js

/// <reference types = "Cypress"/>
class LoginPage

{
    visit(){

        cy.visit('http://wallbee.codebeestaging.com/')
    }

    fillusername(value){
        const field = cy.get('#UserName')
        field.clear()
        field.type(value)
        return this
    }

    fillpassword(value){
        const field = cy.get('#Password')
        field.clear()
        field.type(value)
        return this
    }

    submit(){
        const button = cy.get('.btn')
        button.click()

    }

}

export default LoginPage