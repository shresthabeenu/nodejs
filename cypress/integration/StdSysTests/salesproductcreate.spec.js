/// <reference types = "Cypress"/>
import LoginPage from '../StdSysTests/LoginPage'


describe('select products from account page',()=>{
    before('Login to shipment page',()=>{

        const login = new LoginPage()
        login.visit()
        login.fillusername('wallbeeadmin')
        login.fillpassword('Wallbee123!')
        login.submit()
        cy.title().should('be.equal','WALLBEE - Create Shipment')


    })

    beforeEach('Redirect to the sale product page',()=>{

        cy.visit(Cypress.env('sale_product'))
        // cy.get('.ui-dialog-buttons > .ui-dialog-titlebar > .ui-dialog-titlebar-close').click()
        // cy.get('.Products').trigger('mouseover')
        // cy.get('div.position-fix:nth-child(1) header.head nav.navi div.container-fluid ul.big-nav li.dropdown:nth-child(6) > ul.dropdown-menu').invoke('show')
        // cy.get('.SalesProduct').click()
        
    })

    it ('Create sale product',()=>{
        //cy.get('.page-title > .btn').click()
        cy.get('#InternalName').type('DHL_Freight_SE_API_Stycke_G12')
        cy.get('#DisplayName').type('DHL_Freight_SE_API_Stycke_G12')
        cy.get('#ProductTypeID').select('COURIER')
        cy.get(':nth-child(3) > :nth-child(3) > .switch-button-background > .switch-button-button').click()
        cy.get('#Insurances').type('1000')
        cy.get('#SaleNumber').type('1000')
        cy.get('#MaxWeights').type('1000')
        cy.get('#MaxVolWeights').type('1000')
        cy.get('#MaxColliWeights').type('1000')
        cy.get('#MaxColliVolWeights').type('1000')
        cy.get('#VolumeFactor').type('1000')
        cy.get('#btnSave').click()
        //cy.get('tbody > :nth-child(n)').should('eq', 'tets')
        cy.title().should('be.equal','WALLBEE - Sale Products')

})

})
