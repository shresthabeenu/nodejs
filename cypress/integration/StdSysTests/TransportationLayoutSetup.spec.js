///<reference types = "Cypress" />
import LoginCredentials from '../../support/PageObjects/LoginCredentials'


describe('Select items from transport layout setup page',()=>{

    before(function(){

        cy.fixture('example').then(function(data){
            this.data=data
        })
    })

    beforeEach('Fill the login form and submit',function(){
        
        const lg = new LoginCredentials()
        cy.visit('http://wallbee.codebeestaging.com/')
        lg.username().type(this.data.username)
        lg.password().type(this.data.password)
        cy.get('.btn').click()
        cy.wait(2000)

    })

    it('Select items', ()=>{

            cy.visit(Cypress.env('url')+"/TransportationLayoutSetup/")
            cy.get('#WallTransportationModeId').select('Test')
            cy.wait(5000)
            cy.get(':nth-child(n) > .checkbox-wrap > .wb-checkbox > label', ).filter((index, element)=> index <60).click({ multiple: true, force: true })
            cy.pause()
            cy.get('#btn-save-transportation-layout-setup').click()
            cy.go('back')
          
        })

})



