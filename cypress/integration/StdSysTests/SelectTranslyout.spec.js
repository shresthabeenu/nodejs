
import HomePage from '../../support/pageObjects/HomePage'
import ProductPage from '../../support/pageObjects/ProductPage'
/// <reference types="Cypress" />
import HomePage from '../../support/PageObj/HomePage'
describe('Navigate to history page', () => {

      before(function() {
        // runs once before all tests in the block
        cy.fixture('example').then(function(data)
        {
this.data=data
        })
      })


    before('Login',() => {
    
    //Visit  url
    cy.visit(Cypress.env('url'))
    cy.viewport(1600, 1000)
    //Login
    cy.get('#UserName').type(Cypress.env('username'))
    cy.get('#Password').type(Cypress.env('password'))
    cy.get('.btn').click()
    cy.wait(2000)
})

    before('Redirect to history admin page',() => {
        cy.visit(Cypress.env('trans'))

    })

it('Select all products', ()=>{
    cy.get('#WallTransportationModeId').select('Wallbee-Road')
    cy.wait(5000)
    cy.get(':nth-child(n) > .checkbox-wrap > .wb-checkbox > label', ).filter((index, element)=> index < 10).click({ multiple: true, force: true })
    cy.get('#btn-save-transportation-layout-setup').click()
  
})
})






