import LoginPage from '../StdSysTests/LoginPage'
describe('Alert',()=>{

    before('Login',()=>{

    

        const lp = new LoginPage()
        lp.visit()
        lp.fillusername('wallbeeadmin')
        lp.fillpassword('Wallbee123!')
        lp.submit()
        cy.title().should('be.equal','WALLBEE - Create Shipment')

    })

    it('pop up alert',()=>{
    
        //Click on Branch from the menu bar
        cy.get('.Branch').click()
        cy.wait(2000)
        cy.get('main.container-fluid:nth-child(2) div.page-wrapper div.table-responsive table.table tr:nth-child(9) td.filter-action-btns.action:nth-child(2) ul.action-list li.details-link:nth-child(1) > a:nth-child(1)').click()
        cy.on('window:popup',(str)=>{
            cy.log('test')
            expect(str).to.equal('Branch')
            cy.log('test')

        })

    })


})