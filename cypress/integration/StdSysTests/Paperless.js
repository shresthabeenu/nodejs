describe('Create paperless shipment from clone shipment', () =>{

    before(function() {
        cy.Login()
        cy.viewport(1500,1000)
    })

    beforeEach('Navigate to clone shipment page',() => {

        cy.visit(Cypress.env('clone_ups'))

    })

    beforeEach('Verify clone page navigation',() => {

        cy.url().should('include', 'CloneAndDelete=false')
        cy.log('The clone page is navigated')

    })

    beforeEach('Add commodity on paperless', () => {

        cy.get('main.container-fluid:nth-child(2) div.shipment:nth-child(3) div.row:nth-child(16) div.content-right div.page-wrapper.shipment-form.paperlesscontainer.details_show:nth-child(4) div.row.form-line.sendpaperlesscontainer:nth-child(2) div.form-group.col-md-6.uploadpaperlessinvoicecheckbox.details_show:nth-child(2) div.switch-button-background.checked:nth-child(4) > div.switch-button-button').click()
        cy.get('.newcommodity').within(() => {
            
        //Enter Harmonized Code	
        cy.get('.editor-field').eq(0).click().type('1')
        //Enter Commodity
        cy.get('.editor-field').eq(1).click().type('2')
        cy.wait(5000)

        //Enter Quantity
        cy.get('.editor-field').eq(3).click().type('3')
        //Enter	net weight
        cy.get('.editor-field').eq(4).click().type('4')
        //Enter	Unit Price
        cy.get('.editor-field').eq(6).click().type('5')
        
        })

        cy.get('.InvoiceCommodityDetailsTemplate > :nth-child(12) > .editor-field > .switch-button-background').click()

        //cy.get('.ui-menu').children('li.ui-menu-item').contains('METAL BRACKET FOR TEST').click()

        cy.get('#btnShowShippingOptions').click()

    })

    it('create shipment', () => {

        cy.get('#Shipment_ListOfShipmentBillInfo_9__isSelected').check()
        cy.get('#SubmitButtonContainer > .btn').click()
        cy.url().should('include','ShipmentAWB?ids=')

    })

    /*it('Create shipment with invoice file', () => {

        cy.get('#Shipment_PaperlessFile').click()

    })*/
})
