describe('Navigate to history page', () => {

    before('Login',() => {
    
    //Visit  url
    cy.visit(Cypress.env('url'))
    cy.viewport(1600, 1000)
    //Login
    cy.get('#UserName').type(Cypress.env('username'))
    cy.get('#Password').type(Cypress.env('password'))
    cy.get('.btn').click()
    cy.wait(2000)
})

    before('Redirect to history admin page',() => {
        
        cy.get('.HistoryRoot').trigger('mouseover')
        cy.get('.sub-menu').should('be.hidden').invoke('show')
        cy.get('.Shipment').contains('History').click()
        cy.wait(3000)

    })

        /*before('verify history page navigation',() => {

            cy.url().should('include', 'http://wallbee.codebeestaging.com/Shipment/HistoryAdmin')
            cy.log('History page is navigated')

            })*/

        /*beforeEach('Filter shipment of 2020/Jan/01 till 2021/Jan/01',() =>{

            cy.get('#ShipFrom').click()
            cy.get('#ShipFrom').clear()
            cy.get('#ShipFrom').click()
            cy.get('#ShipFrom').type('01-01-2020')
            cy.get('.advance-filter-body > :nth-child(5)').click()
            cy.get('#BranchID').select('All').should('be.visible').and('contain','All').screenshot()
            cy.get('#filter').click()
            cy.wait(2000)
            
        })

        beforeEach('Text Filter',() =>{

                cy.get('.select2-search__field').click()
                cy.get('ul.select2-results__options').children().contains('3rd Party DDP').click()
            })

        beforeEach('Search shipment with AWB no.',()=>{
            
            cy.get('#SearchString').type('JD011100003642818432')
            cy.get('#filter').click()
            cy.get('.table-bordered').contains('tr.rowclick td', 'JD011100003642818432')
            cy.log("The shipment with 'JD011100003642818432' awb no. is found")
        })*/

        beforeEach('Clone shipment',()=>{

            //tnt

            cy.get('.dropdown').invoke('show')
            cy.get('main.container-fluid:nth-child(2) div.page-wrapper.table-responsive:nth-child(4) table.table.table-bordered:nth-child(1) tr.rowclick:nth-child(8) td.action:nth-child(11) ul.action-list li:nth-child(7) div.dropdown > button.btn.btn-default.dropdown-toggle').click()
            cy.get(':nth-child(8) > .action > ul.action-list > :nth-child(7) > .dropdown > .dropdown-menu > .clone-link > [href="#"] > .fa').trigger('mouseover')
            cy.get('.sub-action-list').invoke("show")
            cy.get(':nth-child(8) > .action > ul.action-list > :nth-child(7) > .dropdown > .dropdown-menu > .clone-link > .sub-action-list > :nth-child(1) > a').click()


            //gls

            /*cy.get(':nth-child(3) > .action > ul.action-list > :nth-child(7) > .dropup > .btn').invoke('show')
            cy.get(':nth-child(3) > .action > ul.action-list > :nth-child(7) > .dropup > .btn > .caret').click()
            cy.get(':nth-child(3) > .action > ul.action-list > :nth-child(7) > .dropup > .dropdown-menu > .clone-link > [href="#"]').trigger('mouseover')
            cy.get('.sub-action-list').invoke("show")
            cy.get(':nth-child(3) > .action > ul.action-list > :nth-child(7) > .dropup > .dropdown-menu > .clone-link > .sub-action-list > :nth-child(1) > a').click()
            
            
            //ups

            //cy.get('main.container-fluid:nth-child(2) div.page-wrapper.table-responsive:nth-child(4) table.table.table-bordered:nth-child(1) tbody:nth-child(1) tr.rowclick:nth-child(2) > td.action:nth-child(11)').invoke('show')
            cy.get('main.container-fluid:nth-child(2) div.page-wrapper.table-responsive:nth-child(4) table.table.table-bordered:nth-child(1) tr.rowclick:nth-child(8) td.action:nth-child(11) ul.action-list li:nth-child(7) div.dropdown > button.btn.btn-default.dropdown-toggle').invoke('show')
            cy.get('').click()
            cy.get('').trigger('mouseover')
            cy.get('.sub-action-list').invoke("show")
            cy.get('').click()*/

        })

        beforeEach('Verify clone page navigation',() => {

            cy.url().should('include', 'CloneAndDelete=false')
            cy.log('The clone page is navigated')

        })

        beforeEach('Select Dangerous Goods',() => 
        {
            cy.get('.DangerousGoodsContainer > .form-group > .switch-button-background > .switch-button-button').click()
            cy.get('#Shipment_Quantity').type('1')
            cy.get('#Shipment_UnNumber').type('1')
            cy.get('#Shipment_DangerousGoodsTypeID').select('2')
        })

        it('create shipment', () => {

            cy.get('.paperlesscontainer > :nth-child(2) > :nth-child(1) > .switch-button-background > .switch-button-button').click()
            cy.get('#btnShowShippingOptions').click()
            cy.wait(5000)
            cy.scrollTo('bottom', {
                duration: 10000,
                easing: 'swing',
            })
            cy.get('.table').should('be.visible').contains('tr.category-1 td','TNT_Economy').click()
            cy.wait(5000)
            cy.get('#Shipment_ListOfShipmentBillInfo_3__isSelected').check()
            cy.get('#SubmitButtonContainer > .btn').click()
            cy.url().should('include','ShipmentAWB?ids=')
        })




        

        

        })
        


    