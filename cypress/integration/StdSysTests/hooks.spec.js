describe('Login with two different users',() => {

    before('before',() => {

        
        cy.log('Navigated to login page')

    })

    after('after', () => {

        
        cy.log('Successfully logged out from customer')

        

    })

    beforeEach('beforeeach', () => {
        cy.visit('http://wallbee.codebeestaging.com/Account/LogOn')
        cy.log('Logged in successfully')
        

    })


    afterEach('aftereach', () => {

        cy.get('div.position-fix:nth-child(1) header.head div.container-fluid div.navbar-top:nth-child(1) div.navbar--user:nth-child(2) div.name div.dropdown strong.dropdown-toggle > span.caret').click()
        cy.get('.logout').click()
        cy.log('Successfully logged out from admin')


    })


    it('Login from admin', () => {

        cy.Login_admin()
        cy.log('Login successful from admin')

    })

    it('test2', () => {

        cy.Login_nor()
        cy.log('Login successful from customer')
        
        
    })
})