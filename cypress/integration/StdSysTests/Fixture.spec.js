describe('Login',()=>{

    before(function(){

        cy.fixture('example').then(function(data){
            this.data=data
        })
    })

    it('Fill the login form and submit',function(){
        
        cy.visit('http://wallbee.codebeestaging.com/')
        cy.get('#UserName').type(this.data.username)
        cy.get('#Password').type(this.data.password)
        cy.get('.btn').click()
        cy.wait(2000)

    })

})