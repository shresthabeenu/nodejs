describe ('Viewport',() =>{

    before (()=>{

        console.log('Running my tests')

    })

    beforeEach (()=>{

        cy.visit('www.google.com')
    })

    it ('open in macbook-11', () => {

        cy.viewport('macbook-11')
        cy.screenshot()
        cy.wait(2000)

    })

    it ('open in samsung-note9', () => {

        cy.viewport('samsung-note9')
        cy.screenshot()
        cy.wait(2000)

    })
    
    

})