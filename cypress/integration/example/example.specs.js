/// <reference types = "Cypress"/>
//import LoginPage from '../StdSysTests/LoginPage'
import { Given } from "cypress-cucumber-preprocessor/steps";
import Login from '../../support/PageObjects/Login'
const login = new Login ()
Given('I open the Login page', () => {
  login.visit()
})

And ('I type',(datatable) => {
    datatable.hashes().forEach(element => {
        login.username(element.username)
        login.password(element.password)
    })
})
    When ('I click on the Login button',() => {

     login.loginbtn

    })


